import * as THREE from 'three';
import start from './boot';

const FPS_TIME = 60 / 1000;
const SCALE_MAX = 20;
const FADE_OUT = -0.05 * FPS_TIME;
const FREQUENCY = 70;
const COL_MIN = -0.1;
const COL_MAX = 0.2;
const COL_VARIANCE = COL_MAX - COL_MIN;
const UNIT = 10;
const COLORS = [
  0xdd2299, // purple
  // 0xdd9922, // yellowish
  // 0xff0000, // red
  // 0x999999, // grey
];

const hRad = () => UNIT + Math.random();
const vRad = () => UNIT + Math.random();
const hRadInc = () => 0.01 * FPS_TIME;
const vRadInc = () => 0.01 * FPS_TIME;
const lrSpeed = () => 0.1 + Math.random() / 5;
const scaleUpSpeed = () => 0.02 * FPS_TIME;
const nextColor = () => {
  const colorHex = COLORS[Math.floor(Math.random() * COLORS.length)];
  const offsetL = COL_MIN + Math.random() * COL_VARIANCE;

  const color = new THREE.Color(colorHex);
  color.offsetHSL(0, 0, offsetL);
  
  return color;
};

function createCircle() {
  return {
    scaleSpeed: scaleUpSpeed(),
    lrSpeed: lrSpeed(),
    hRad: hRad(),
    vRad: vRad(),
    hRadInc: hRadInc(),
    vRadInc: vRadInc(),
    lr: 0,
    expand(time) {
      const { position, scale } = this.$mesh;

      this.lr += time * this.lrSpeed;
      this.hRad += time * this.hRadInc;
      this.vRad += time * this.vRadInc;
      const x = UNIT + this.hRad * Math.sin(this.lr * Math.PI/180);
      const y = UNIT + this.vRad * Math.cos(this.lr * Math.PI/180);
      const scaleX = scale.x * (1 + time * this.scaleSpeed);
      const scaleY = scale.y * (1 + time * this.scaleSpeed);

      position.set(x, y, position.z);
      scale.set(scaleX, scaleY, scale.z);

      if (scale.x > SCALE_MAX) {
        this.fade(time);
      }
    },
    fade(time) {
      const { material } = this;

      material.opacity *= (1 + time * FADE_OUT);

      if (material.opacity < 0.1) {
        this.$destroy();
      }
    },
    $create() {
      const geometry = new THREE.CircleGeometry( 5, 64 );
      const material = new THREE.MeshBasicMaterial( { color: nextColor() } );
      material.transparent = true;
      material.opacity = 1;

      this.material = material;

      return new THREE.Mesh( geometry, this.material );
    },
    $onEnterFrame(time) {
      this.expand(time);
    },
  };
}

start(({ camera, onEnterFrame, addObject }) => {
  camera.position.z = 200;

  onEnterFrame(() => {
    if (Math.floor(Math.random() * FREQUENCY) < 1) {
      addObject(createCircle());
    }
  });
});
