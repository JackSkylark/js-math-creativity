# js-math-creativity

This is a JS port of the book ["Flash Math Creativity"](https://www.amazon.com/Flash-Math-Creativity-Keith-Peters/dp/1590594290).

## How do I run this?

1. `yarn install`
2. `yarn run dev`
3. Visit `http://localhost:8080/towards-us.html`
4. Visit other demos `http://localhost:8080/${js_file_name}.html`
