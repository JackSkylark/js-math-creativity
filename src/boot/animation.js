export default function startAnimation({ renderer, scene, camera }) {
  const listeners = new Set();
  let lastTime = 0;

  function animate(){
    const time = Date.now();
    const delta = Math.floor(lastTime && time - lastTime);
    listeners.forEach(fn => fn(delta));
    lastTime = time;

    renderer.render(scene, camera);        

    requestAnimationFrame(animate);
  }

  animate();
  
  return {
    onEnterFrame(fn) {
      listeners.add(fn);

      return () => {
        this.offEnterFrame(fn);
      };
    },
    offEnterFrame(fn) {
      listeners.delete(fn);
    },
  };
}
