const { PerformanceObserver, performance } = require('perf_hooks');

const INSERT_ITERATIONS = 10000;
const MAIN_ITERATIONS = 50000;

function generateId() {
  return Math.random().toString(36).substr(2, 16);
}

function createObject() {
  let x = 0;
  const inc = Math.floor(Math.random() * 5);

  return {
    update(registry) {
      x += inc;

      if (x > 100) {
        registry.remove(this);
      }
    }
  }
}

function createObjectWithId() {
  let x = 0;
  const inc = Math.floor(Math.random() * 5);

  return {
    id: generateId(),
    update(registry) {
      x += inc;

      if (x > 100) {
        registry.remove(this);
      }
    }
  }
}

function createArrayImpl() {
  const objects = [];
  let maxCount = 0;

  return {
    add(obj) {
      objects.push(obj);
      maxCount = Math.max(maxCount, objects.length);
    },

    remove(obj) {
      const idx = objects.indexOf(obj);

      if (idx > -1) {
        objects.splice(idx, 1);
      }
    },

    update() {
      objects.forEach(x => x.update(this));
    },

    maxCount() {
      return maxCount;
    }
  };
}

function createObjectImpl() {
  const objects = {};
  let count = 0;
  let maxCount = 0;
  
  return {
    add(obj) {
      objects[obj.id] = obj;
      count++;
      maxCount = Math.max(maxCount, count);
    },

    remove(obj) {
      if (objects[obj.id]) {
        delete objects[obj.id];
        count--;
      }
    },

    update() {
      Object.values(objects).forEach(x => x.update(this));
    },

    maxCount() {
      return maxCount;
    },
  };
}

function createSetImpl() {
  const objects = new Set();
  let maxCount = 0;

  return {
    add(obj) {
      objects.add(obj);
      maxCount = Math.max(maxCount, objects.size);
    },

    remove(obj) {
      objects.delete(obj);
    },

    update() {
      objects.forEach(x => x.update(this));
    },

    maxCount() {
      return maxCount;
    },
  }
}

function createMapImpl() {
  const objects = new Map();
  let maxCount = 0;

  return {
    add(obj) {
      objects.set(obj.id, obj);
      maxCount = Math.max(maxCount, objects.size);
    },

    remove(obj) {
      objects.delete(obj.id);
    },

    update() {
      objects.forEach(x => x.update(this));
    },

    maxCount() {
      return maxCount;
    },
  }
}

function testImpl(testName, createRegistryFn, createObjectFn) {
  performance.mark(`${testName} - start`);

  const registry = createRegistryFn();

  console.log(`[${testName}] running insert iterations (${INSERT_ITERATIONS})...`);
  for (var i = 0; i < INSERT_ITERATIONS; i++) {
    registry.add(createObjectFn());
  }

  console.log(`[${testName}] running main iterations (${MAIN_ITERATIONS})...`);
  for (var i = 0; i < MAIN_ITERATIONS; i++) {
    if (Math.random() > 0.5) {
      registry.add(createObjectFn());
    }
    registry.update();
  }

  console.log(`[${testName}] max object size: ${registry.maxCount()}`);

  performance.mark(`${testName} - end`);
  performance.measure(testName, `${testName} - start`, `${testName} - end`);

}

function main() {
  const obs = new PerformanceObserver((items) => {
    const entry = items.getEntries()[0];
    console.log(`[${entry.name}] duration: ${entry.duration}`);
    console.log('-----------------------------');
    performance.clearMarks();
  });
  obs.observe({ entryTypes: ['measure'] });

  testImpl('Array Implementation', createArrayImpl, createObject);
  testImpl('Set Implementation', createSetImpl, createObject);
  testImpl('Map Implementation', createMapImpl, createObjectWithId);
  testImpl('Set Implementation with Object Id', createSetImpl, createObjectWithId);
  // testImpl('Object Implementation', createObjectImpl, createObjectWithId);
}

main();
