import * as THREE from 'three';
import start from './boot';

start(({ camera, addObject }) => {
  camera.position.z = 5;

  let count = 0;

  addObject({
    speed: Math.PI / 2000,
    $create() {
      const geometry = new THREE.BoxGeometry(3, 3, 3);
      const material = new THREE.MeshBasicMaterial({ color: 0x22dd99, wireframe: true });
      return new THREE.Mesh(geometry, material);
    },
    $onEnterFrame(time) {
      const offset = time * this.speed;

      this.$mesh.rotation.x += offset;
      this.$mesh.rotation.y += offset;
    },
  });
});
