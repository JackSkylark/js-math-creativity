const HtmlWebpackPlugin = require('html-webpack-plugin');

function appConfig(name) {
  return {
    entry: `./src/${name}.js`,
    output: {
      path: `${__dirname}/dist`,
      filename: `${name}.bundle.js`
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: name,
        filename: `${name}.html`,
        template: `template.html`,
      })
    ]
  };
}

module.exports = [
  appConfig('hello-cube'),
  appConfig('towards-us'),
];
